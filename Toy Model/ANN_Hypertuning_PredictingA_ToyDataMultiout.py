#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)


import scipy.interpolate as interp
#import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
logdir = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
shutil.rmtree(logdir, ignore_errors=True)

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#gpu configuration:

gpu_num = 0

gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")


# In[4]:


import matplotlib.pyplot as plt
import numpy as np

from sklearn.svm import SVR
from sklearn.multioutput import MultiOutputRegressor


# In[5]:


df = pd.read_csv('polynomial3.csv')


# In[6]:


df = df.iloc[:8000]
train_dataset = df.sample(frac=0.8, random_state=1)
test_dataset = df.drop(train_dataset.index)


# In[7]:


train_features = train_dataset.copy()
test_features = test_dataset.copy()


# In[22]:


train_featuress = train_features.drop(columns = ['a','b', 'c', 'd'], axis = 1)
test_featuress = test_features.drop(columns = ['a', 'b', 'c', 'd'], axis = 1)


# In[23]:


train_labels = train_features.drop(columns = ['x1', 'y1', 'x2', 'y2'], axis = 1)
test_labels = test_features.drop(columns = ['x1', 'y1', 'x2', 'y2'], axis = 1)


# In[24]:


import pandas as pd 
import numpy as np 
from tensorflow import keras 
from tensorflow.keras import layers 
from sklearn.model_selection import train_test_split


# In[25]:


from tensorflow import keras
from keras import models, layers


# In[26]:


a_train = np.array(train_features["a"])
a_test = np.array(test_features["a"])


# In[16]:


train_featuress.shape[1]


# In[ ]:


import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Dropout, LayerNormalization
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import regularizers
import keras_tuner as kt

def build_model(hp):
    input_layer = Input(shape=(train_featuress.shape[1],))

    use_normalization = hp.Boolean('use_normalization')
    if use_normalization:
        x = LayerNormalization()(input_layer)
    else:
        x = input_layer

    num_layers = hp.Int('num_layers', min_value=1, max_value=6)

    for i in range(num_layers): 
        units = hp.Int(f'units_{i}', min_value=16, max_value=256, step=4)
        activation = hp.Choice(f'activation_{i}', values=['relu', 'tanh', 'exponential', 'elu', 'linear'])
        
        l1 = hp.Float('l1', min_value=0.0, max_value=0.1, step=0.01)
        l2 = hp.Float('l2', min_value=0.0, max_value=0.1, step=0.01)
        
        x = Dense(units=units, activation=activation,
                  kernel_regularizer=regularizers.l1_l2(l1=l1, l2=l2))(x)
        
        if i < num_layers-1: 
            dropout_rate = hp.Float(f'dropout_{i}', min_value=0.0, max_value=0.5, step=0.1)
            x = Dropout(rate=dropout_rate)(x)

    a_out = Dense(units=1, name='a_out')(x)

    model = Model(inputs=input_layer, outputs=a_out)
    
    learning_rate = hp.Float('learning_rate', min_value=1e-5, max_value=0.1, sampling='log')
    
    loss_choice = hp.Choice('loss', values=['mse', 'mae', 'huber'])

    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    
    model.compile(optimizer=optimizer,
                  loss=loss_choice,
                  metrics=[tf.keras.metrics.RootMeanSquaredError(), tf.keras.metrics.MeanAbsoluteError()])
    
    return model

tuner = kt.Hyperband(
    build_model,
    objective='val_root_mean_squared_error',  
    max_epochs=100,  
    factor=3,  
    hyperband_iterations=15,  
    directory='kt_tuner_dir',
    project_name='tune_activation_norm_loss', 
    overwrite=True, 
    max_consecutive_failed_trials = 200, 
)


early_stopping = EarlyStopping(monitor='val_loss', patience=40, restore_best_weights=True, verbose=1)

tuner.search(train_featuress, a_train, 
             epochs=400,
             batch_size=8,
             validation_split=0.2,
             callbacks=[early_stopping],
             verbose=1)

best_hp = tuner.get_best_hyperparameters(num_trials=1)[0]
print("Best hyperparameters:", best_hp.values)

# Save the best hyperparameters to a text file
with open('best_hyperparameters1.txt', 'w') as f:
    f.write(str(best_hp.values))
best_model = tuner.get_best_models(num_models=1)[0]

best_model.save('best_model.h5')
print("Best model saved to 'best_model.h5'")

# In[ ]:





# In[ ]:




