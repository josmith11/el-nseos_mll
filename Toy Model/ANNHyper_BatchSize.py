import matplotlib.pyplot as plt
import pandas as pd

#import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
# Make NumPy printouts easier to read.
#gpu configuration:

gpu_num = 1

#gpus = tf.config.experimental.list_physical_devices('GPU')
gpus = tf.config.list_physical_devices('GPU')
print(f"Available GPUs: {gpus}")
if gpus:
    try:
        
        tf.config.experimental.set_memory_growth(gpus[gpu_num], True)
        #Currentli, memory growth needs to be the sime across GPU
        tf.config.experimental.set_virtual_device_configuration(
            gpus[gpu_num],
            [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=32768)]
        )
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")


# In[2]:


import matplotlib.pyplot as plt
import numpy as np

from sklearn.svm import SVR
from sklearn.multioutput import MultiOutputRegressor


# In[3]:


df = pd.read_csv('polynomial3.csv')


# In[4]:


#df = df.iloc[:8000]
train_dataset = df.sample(frac=0.8, random_state=1)
test_dataset = df.drop(train_dataset.index)


# In[5]:


train_features = train_dataset.copy()
test_features = test_dataset.copy()


# In[6]:


train_featuress = train_features.drop(columns = ['a','b', 'c', 'd'], axis = 1)
test_featuress = test_features.drop(columns = ['a', 'b', 'c', 'd'], axis = 1)


# In[7]:


train_labels = train_features.drop(columns = ['x1', 'y1', 'x2', 'y2'], axis = 1)
test_labels = test_features.drop(columns = ['x1', 'y1', 'x2', 'y2'], axis = 1)


# In[8]:


import pandas as pd 
import numpy as np 
from tensorflow import keras 
from tensorflow.keras import layers 
from sklearn.model_selection import train_test_split


# In[9]:


from tensorflow import keras
from keras import models, layers


# In[10]:


a_train = np.array(train_features["a"])
a_test = np.array(test_features["a"])


# In[15]:


import tensorflow as tf
from tensorflow.keras.layers import Input, Dense, Dropout, LayerNormalization
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import regularizers
import keras_tuner as kt

class MyHyperModel(kt.HyperModel):
    def build(self, hp):

        input_layer = Input(shape=(train_featuress.shape[1],))
        use_normalization = hp.Boolean('use_normalization')

        if use_normalization:
            x = LayerNormalization()(input_layer)
        else:
            x = input_layer

        num_layers = hp.Int('num_layers', min_value=1, max_value=10)

        for i in range(num_layers):

            units = hp.Choice(f"units_{i}", [2,4,8,16, 32, 64, 128,256,512, 1024])
            activation = hp.Choice(f'activation_{i}', values=['relu', 'tanh', 'exponential', 'elu', 'linear', 'selu', 'leaky_relu', 'hard_silu', 'gelu'])
        
            l1 = hp.Float(f'l1_{i}', min_value=0.0, max_value=0.1, step=0.01)
            l2 = hp.Float(f'l2_{i}', min_value=0.0, max_value=0.1, step=0.01)
        
            x = Dense(units=units, activation=activation, 
                kernel_regularizer=regularizers.l1_l2(l1=l1, l2=l2))(x)

            if i < num_layers - 1: 
                dropout_rate = hp.Float(f'dropout_{i}', min_value=0.0, max_value=0.5, step=0.05)
                x = Dropout(rate=dropout_rate)(x)

        activationout = hp.Choice('activationout', values=['relu', 'tanh', 'exponential', 'elu', 'linear', 'selu', 'leaky_relu', 'hard_silu', 'gelu'])

        a_out  = Dense(units = 1, activation = activationout, name = 'a_out')(x)

        model = Model(inputs = input_layer, outputs = a_out)
        loss_choice = hp.Choice('loss', values=['mse', 'mae', 'huber'])
        
        model.compile(
            optimizer=tf.keras.optimizers.Adam(
                learning_rate=hp.Float("learning_rate", 1e-5, 0.1, sampling="log")
            ),

            loss=loss_choice,
            metrics=[tf.keras.metrics.RootMeanSquaredError()],
        )
        return model

    def fit(self, hp, model, *args, **kwargs):
        return model.fit(
            *args,
            batch_size=hp.Choice("batch_size", [2,4,8,16, 32, 64, 128]),
            **kwargs,
        )

tuner = kt.Hyperband(
    MyHyperModel(),
    objective='val_root_mean_squared_error',
    max_epochs=100,
    factor=3,
    hyperband_iterations=15,
    directory='kt_tuner_dir',
    project_name='tune_activation_norm_loss',
    overwrite=True, 
    max_consecutive_failed_trials = 200, 
)

tuner.search(
    train_featuress,
    a_train,
    validation_split=0.2,
    epochs=400,
    callbacks=[
        tf.keras.callbacks.ModelCheckpoint("model_checkpoint1.keras", save_best_only=True),
        tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=65, restore_best_weights=True)
    ],
    verbose=1,
)
best_hp = tuner.get_best_hyperparameters(1)[0]
print("Best hyperparameters:", best_hp.values)

best_model = tuner.hypermodel.build(best_hp)
best_model.save("best_model_tuned_batch_size.h5")
print("Best model saved as best_model_tuned_batch_size.h5")

with open("best_hyperparameters_tuned_batch_size.txt", "w") as f:
    f.write(str(best_hp.values))


