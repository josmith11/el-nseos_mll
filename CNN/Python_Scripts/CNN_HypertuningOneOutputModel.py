#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)


import scipy.interpolate as interp
import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
logdir = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
shutil.rmtree(logdir, ignore_errors=True)

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#gpu configuration:

gpu_num = 0

gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
import lal


# In[2]:


df = pd.read_csv('preprocessing.csv', index_col = 0)
df


# normeddata = pd.DataFrame().reindex(columns=df.columns)
# normeddata

# In[3]:


normeddata = df.copy(deep = True )
normeddata


# In[4]:


m1norm = (df['m1'] - df['m1'].mean())/ df['m1'].std()
m2norm = (df['m2'] - df['m2'].mean())/ df['m2'].std()
l1norm = (df['l1'] - df['l1'].mean())/ df['l1'].std()
l2norm = (df['l2'] - df['l2'].mean())/ df['l2'].std()
normeddata['m1'] = m1norm
normeddata['m2'] = m2norm
normeddata['l1'] = l1norm
normeddata['l2'] = l2norm 
normeddata


# In[5]:


def normalizeandwhiten(x):
    gamma0_norm = (x['gamma0'] - 1) / (5 - 1)
    gamma1_norm = (x['gamma1'] - 1) / (5 - 1)
    gamma2_norm = (x['gamma2'] - 1) / (5 - 1)
    #normalizating p values
    logp1_cgs_norm = (x['logp1_cgs'] - 33) / (37 - 33)
    logp2_cgs_norm = (x['logp2_cgs'] - 33) / (37 - 33)
    return gamma0_norm, logp1_cgs_norm, gamma1_norm, logp2_cgs_norm, gamma2_norm
#print(gamma0_norm)
# Call the function and save the normalized values to 'normeddata'
normeddata['gamma0'], normeddata['logp1_cgs'], normeddata['gamma1'], normeddata['logp2_cgs'], normeddata['gamma2'] = normalizeandwhiten(normeddata)


# In[6]:


train_dataset = normeddata.sample(frac=0.8, random_state=1)
test_dataset = normeddata.drop(train_dataset.index)


# In[7]:


train_stats = train_dataset.describe()
train_stats = train_stats.transpose()


# In[8]:


train_copy = train_dataset.copy()
test_copy = test_dataset.copy()


# In[9]:


train_label = train_copy.drop(columns = ['m1', 'm2', 'l1', 'l2']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 large training
test_label = test_copy.drop(columns = ['m1', 'm2', 'l1', 'l2']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 small testing

train_features = train_copy.drop(columns = ['gamma0', 'gamma1', 'gamma2', 'logp1_cgs', 'logp2_cgs']) #m1, m2, l1,l2 large
test_features = test_copy.drop(columns = ['gamma0', 'gamma1', 'gamma2', 'logp1_cgs', 'logp2_cgs']) #m1,m2,l1,l2 small


# In[10]:


import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv1D, GlobalMaxPooling1D, AveragePooling1D, MaxPooling1D, Flatten, Dense, ReLU
from keras.layers import BatchNormalization, LayerNormalization
from keras import regularizers

import tensorflow as tf
from tensorflow import keras
import keras_tuner as kt
from keras_tuner import HyperParameters, BayesianOptimization
from keras.regularizers import l2
import keras_tuner
import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv1D, Dropout, GlobalMaxPooling1D, MaxPooling1D, Dense
from tensorflow.keras.layers import LayerNormalization
import tensorflow.keras.optimizers as optimizers


# In[11]:


callback = tf.keras.callbacks.EarlyStopping(monitor='loss',
                                 patience=200,
                                 min_delta=0.001, 
                                 mode='auto') 


# In[145]:


from keras.layers import Input, Conv1D, ReLU, MaxPooling1D, AveragePooling1D, GlobalMaxPooling1D, Dense, Flatten
from keras.models import Model
from keras.optimizers import SGD
import keras_tuner as kt

class MyHyperModel(kt.HyperModel):
    def build(self, hp):
        act = ['selu', 'elu', 'leaky_relu', 'relu6', 'gelu', 'sigmoid', 'linear']
        learning_rate_choices = [10**i for i in range(-5, 0)]
        hp_learning_rate = hp.Choice('learning_rate', values=learning_rate_choices)
        inputs = Input(shape=(4, 1))  # Adjusted input shape for compatibility
        x = inputs

        # Iterate a number of conv blocks from min_value to max_value
        for i in range(hp.Int('conv_blocks', min_value=1, max_value=5, default=1)):
            filters = hp.Int('filters_' + str(i), min_value=16, max_value=64, step=8)

            # Define the conv and activation layers for each block
            for _ in range(2):
                x = Conv1D(filters, kernel_size=3, padding='same')(x)
                x = ReLU()(x)

            # Choose an optimal pooling type and size
            pool_type = hp.Choice('pooling_' + str(i), ['avg', 'max', 'global_max'])
            if pool_type == 'max':
                pool_size = hp.Int('pool_size_' + str(i), min_value=2, max_value=4, step=1)
                x = MaxPooling1D(pool_size=pool_size, padding='same')(x)
            elif pool_type == 'avg':
                pool_size = hp.Int('pool_size_' + str(i), min_value=2, max_value=4, step=1)
                x = AveragePooling1D(pool_size=pool_size, padding='same')(x)
            elif pool_type == 'global_max':
                x = GlobalMaxPooling1D()(x)

        x = Flatten()(x)
        
        # Tune the number of units in the Dense layer
        x = Dense(hp.Int('Dense_units', min_value=16, max_value=126, step=10, default=32), activation=hp.Choice('actfuncout', act))(x)
        outputs = Dense(5, activation=hp.Choice('outoutact', act))(x)  # Assuming linear activation for regression

        # Define the model
        model = Model(inputs, outputs)

        # Compile the model
        model.compile(optimizer=SGD(learning_rate=hp_learning_rate),
                      loss='mse', metrics=[keras.metrics.RootMeanSquaredError()])  # Add more metrics as needed
        return model

    def fit(self, hp, model, *args, **kwargs):
        return model.fit(
            *args,
            batch_size=hp.Choice("batch_size", [8, 16, 32]),
            **kwargs,
        )


# In[149]:


from keras import backend as K
import keras

def root_mean_squared_errors(y_true, y_pred):
    return K.sqrt(K.mean(K.sum(K.square(y_pred - y_true), axis=-1)))

# Register the custom metric
keras.utils.get_custom_objects().update({"root_mean_squared_error": root_mean_squared_errors})


# In[152]:


# initialize tuner to run the model.
# using the Hyperband search algorithm

tuner = kt.Hyperband(
    MyHyperModel(),
    objective=  kt.Objective('root_mean_squared_error', direction='min'),#'accuracy', #kt.Objective('cosine_similarity_metric', direction='max'),
    #batch_size = 32,
    hyperband_iterations=500,
    #max_trials=10,#500
    #executions_per_trial = 5,
    
    overwrite=True,
    factor = 50,
    #max_epochs = 20, 
    #max_retries_per_trial = 5, 
    max_consecutive_failed_trials = 100,
    #directory="my_dir",
    #project_name="tune_hypermodel",
    #    objective="val_accuracy",
    #directory='my_dir', 
    #project_name='my_project'
)


# In[ ]:


tuner.search(train_features, 
             train_label,
            # epochs=30,#100
             validation_split=.2, 
             callbacks=[callback])


# In[ ]:


best_hps = tuner.get_best_hyperparameters()[0]
print(best_hps.values)


# In[ ]:


models = tuner.get_best_models(num_models=1)
best_model = models[0]
best_model.save("cnn_hypertuning_july8.keras")


# In[ ]:


# display model structure
plot_model(best_model, 'best_model.png', show_shapes=True)

# show model summary
best_model.summary()


# In[ ]:


test_predictions = models[0].predict(train_features)


# In[ ]:


print(test_predictions)


# In[ ]:


lab_max = normeddata['gamma0'].max()
lab_min = normeddata['gamma0'].min()
lab_range = lab_max - lab_min
lab_mean = normeddata['gamma0'].mean()
print(lab_max, lab_min, lab_range, lab_mean)


# In[ ]:


pred_max = test_predictions[:, [0]].max()
pred_min = test_predictions[:, [0]].min()
pred_range = pred_max - pred_min
pred_mean = test_predictions[:, [0]].mean()
print(pred_max, pred_min, pred_range, pred_mean)


# In[ ]:


predictions_array_length = 40000
numcols = 10
array_sizes =  np.array([[0] * numcols ] * predictions_array_length,dtype='float64')
predictions = pd.DataFrame(array_sizes)
predictions.columns = [ 'gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted', 'gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted','logp2_cgs_predicted', 'gamma2_predicted']


# In[ ]:


predictions['gamma0_norm_predicted'] = test_predictions[:, [0]]
predictions['logp1_cgs_norm_predicted'] = test_predictions[:, [1]]
predictions['gamma1_norm_predicted'] = test_predictions[:, [2]]
predictions['logp2_cgs_norm_predicted'] = test_predictions[:, [3]]
predictions['gamma2_norm_predicted'] = test_predictions[:, [4]]
predictions


# In[ ]:


predictions['gamma0_predicted'] = test_predictions[:, [0]] * (5 - 1) + 1
predictions['logp1_cgs_predicted'] = test_predictions[:, [1]] *  (37 - 33) + 33
predictions['gamma1_predicted'] = test_predictions[:, [2]] * (5 - 1) + 1
predictions['logp2_cgs_predicted'] = test_predictions[:, [3]] *  (37 - 33) + 33
predictions['gamma2_predicted'] = test_predictions[:, [4]] * (5 - 1) + 1
predictions


# In[ ]:


import numpy as np
import matplotlib.pyplot as plt

labels_cols = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']
norm_labels = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']

norm_predictions = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted']
predictions_cols = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']


labels = [df[col].to_numpy() for col in labels_cols]
normlabel = [normeddata[col].to_numpy() for col in norm_labels]

normpredictions = [predictions[col].to_numpy() for col in norm_predictions]
prediction = [predictions[col].to_numpy() for col in predictions_cols]


fig, axs = plt.subplots(5, 4, figsize=(15, 15))

for i in range(5):
    for j in range(4):
        if j == 0:  #first column i want to be gamma0_labels
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(labels[i], color=color, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Labels Unnormalized ({labels_cols[i]}) ")
            
        elif j == 1:  #second column should be the gamm0 predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normlabel[i], color=color,range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Labels ({norm_labels[i]}) ")
    
          
        elif j == 2:  #3rd column should be the norm labels 
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normpredictions[i], color=color,range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Pred. ({norm_predictions[i]}) ")
            
        
        elif j == 3: # 4th column should be the norm predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(prediction[i], color=color,range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Unnormalized Pred. ({predictions_cols[i]}) ")

plt.tight_layout()
plt.subplots_adjust(top=.94) 
fig.suptitle('predicting one output, cosine sim', fontsize=20)
plt.show()


# In[ ]:


import numpy as np
import matplotlib.pyplot as plt

labels_cols = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']
norm_labels = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']

norm_predictions = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted']
predictions_cols = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']


labels = [df[col].to_numpy() for col in labels_cols]
normlabel = [normeddata[col].to_numpy() for col in norm_labels]

normpredictions = [predictions[col].to_numpy() for col in norm_predictions]
prediction = [predictions[col].to_numpy() for col in predictions_cols]


fig, axs = plt.subplots(5, 4, figsize=(15, 15))

for i in range(5):
    for j in range(4):
        if j == 0:  #first column i want to be gamma0_labels
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(labels[i], color=color)#, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Labels Unnormalized ({labels_cols[i]}) ")
            
        elif j == 1:  #second column should be the gamm0 predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normlabel[i], color=color) #range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Labels ({norm_labels[i]}) ")
    
          
        elif j == 2:  #3rd column should be the norm labels 
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normpredictions[i], color=color)#, range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Pred. ({norm_predictions[i]}) ")
            
        
        elif j == 3: # 4th column should be the norm predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(prediction[i], color=color)#, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Unnormalized Pred. ({predictions_cols[i]}) ")

plt.tight_layout()
plt.subplots_adjust(top=.94) 
fig.suptitle('predicting one output, cosine sim', fontsize=20)
plt.show()


# In[ ]:





# In[ ]:





# In[ ]:




