#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)


import scipy.interpolate as interp
#import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
logdir = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
shutil.rmtree(logdir, ignore_errors=True)

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#gpu configuration:

gpu_num = 0

gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
#import lal


# In[2]:


df = pd.read_csv('preprocessing.csv', index_col = 0)
df


# normeddata = pd.DataFrame().reindex(columns=df.columns)
# normeddata

# In[3]:


normeddata = df.copy(deep = True )
normeddata


# In[4]:


def normalizeandwhiten(x):
    gamma0_norm = (x['gamma0'] - 1) / (5 - 1)
    gamma1_norm = (x['gamma1'] - 1) / (5 - 1)
    gamma2_norm = (x['gamma2'] - 1) / (5 - 1)
    #normalizating p values
    logp1_cgs_norm = (x['logp1_cgs'] - 33) / (37 - 33)
    logp2_cgs_norm = (x['logp2_cgs'] - 33) / (37 - 33)
    return gamma0_norm, logp1_cgs_norm, gamma1_norm, logp2_cgs_norm, gamma2_norm
#print(gamma0_norm)
# Call the function and save the normalized values to 'normeddata'
normeddata['gamma0'], normeddata['logp1_cgs'], normeddata['gamma1'], normeddata['logp2_cgs'], normeddata['gamma2'] = normalizeandwhiten(normeddata)


# In[5]:


normeddata


# m1norm  = (df['m1'] - df['m1'].mean())/ df['m1'].std()
# print(m1norm)

# In[6]:


m1norm = (df['m1'] - df['m1'].mean())/ df['m1'].std()
m2norm = (df['m2'] - df['m2'].mean())/ df['m2'].std()
l1norm = (df['l1'] - df['l1'].mean())/ df['l1'].std()
l2norm = (df['l2'] - df['l2'].mean())/ df['l2'].std()
normeddata['m1'] = m1norm
normeddata['m2'] = m2norm
normeddata['l1'] = l1norm
normeddata['l2'] = l2norm 
normeddata.describe()


# m1norm = df['m1']
# 
# m1norm = (df['m1'] - df['m1'].min())/(df['m1'].max()-df['m1'].min())
# m2norm = (df['m2'] - df['m2'].min())/(df['m2'].max()-df['m2'].min())
# l1norm = (df['l1'] - df['l1'].min())/(df['l1'].max()-df['l1'].min())
# l2norm = (df['l2'] - df['l2'].min())/(df['l2'].max()-df['l2'].min())
# normeddata['m1'] = m1norm
# normeddata['m2'] = m2norm
# normeddata['l1'] = l1norm
# normeddata['l2'] = l2norm 
# normeddata

# def whiteninput(x,y):
#     m1norm = (x['y'] - x['y'].mean())/x['y'].std()
#     return m1norm
# print(whiteninput(df, ['m1']))

# In[7]:


train_dataset = normeddata.sample(frac=0.8, random_state=1)
test_dataset = normeddata.drop(train_dataset.index)


# In[8]:


train_stats = train_dataset.describe()
train_stats = train_stats.transpose()


# In[9]:


train_copy = train_dataset.copy()
test_copy = test_dataset.copy()


# In[10]:


train_copy


# In[11]:


train_label = train_copy.drop(columns = ['m1', 'm2', 'l1', 'l2', 'gamma1', 'logp1_cgs', 'logp2_cgs', 'gamma2']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 large training
test_label = test_copy.drop(columns = ['m1', 'm2', 'l1', 'l2', 'gamma1', 'logp1_cgs', 'logp2_cgs', 'gamma2']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 small testing

train_features = train_copy.drop(columns = ['gamma0', 'gamma1', 'gamma2', 'logp1_cgs', 'logp2_cgs']) #m1, m2, l1,l2 large
test_features = test_copy.drop(columns = ['gamma0', 'gamma1', 'gamma2', 'logp1_cgs', 'logp2_cgs']) #m1,m2,l1,l2 small


# In[12]:


train_features


# import tensorflow as tf
# import pandas as pd
# 
# class Normalize(tf.Module):
#     def __init__(self, x):
#         # Convert x to NumPy array if it's a DataFrame
#         if isinstance(x, pd.DataFrame):
#             x = x.values
# m        
#         # get mean and standard dev
#         self.mean = tf.Variable(tf.reduce_mean(x, axis=0))
#         self.std = tf.Variable(tf.math.reduce_std(x, axis=0))
# 
#     def norm(self, x):
#         # Convert x to NumPy array if it's a DataFrame
#         if isinstance(x, pd.DataFrame):
#             x = x.values
#         
#         # normalize input
#         return (x - self.mean) / self.std
# 
#     def unnorm(self, x):
#         # Convert x to NumPy array if it's a DataFrame
#         if isinstance(x, pd.DataFrame):
#             x = x.values
#         
#         # unnorm input
#         return (x * self.std) + self.mean
# 
# # Example usage with train_features (assuming train_features is a Pandas DataFrame)
# 
# norm_x = Normalize(train_features.values)  # Pass NumPy array representation
# 
# # Normalize train_features
# x_train_norm = norm_x.norm(train_features)
# 
# # Print normalized features
# print("Normalized features:")
# print(x_train_norm)
# 
# # Unnormalize (optional)
# x_train_unnorm = norm_x.unnorm(x_train_norm)
# print("\nUnnormalized features:")
# print(x_train_unnorm)
# 

# class Normalize(tf.Module):
#   def __init__(self, x):
#     # get mean and standard dev
#     self.mean = tf.Variable(tf.math.reduce_mean(x, axis=0))
#     self.std = tf.Variable(tf.math.reduce_std(x, axis=0))
# 
#   def norm(self, x):
#     # normalize input
#     return (x - self.mean)/self.std
# 
#   def unnorm(self, x):
#     # unnorm input
#     return (x * self.std) + self.mean
# 
# #orm_x = Normalize(df)
# norm_x = Normalize(train_features)  # Use .values to pass the NumPy array representation
# 
# x_train_norm = norm_x.norm(train_features)

# In[13]:


train_label


# In[14]:


import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv1D, GlobalAveragePooling1D, Flatten, Dense
from keras.layers import BatchNormalization, LayerNormalization
from keras import regularizers

import tensorflow as tf
from tensorflow import keras
import keras_tuner as kt
from keras_tuner import HyperParameters, BayesianOptimization
from keras.regularizers import l2
import keras_tuner
import keras


# In[15]:


#custom objective function
#def rmse(y_true, y_pred):
    #return np.sqrt(((y_true - y_targ) ** 2).mean())


# train_features = np.reshape(train_features, (train_features.shape[0], train_features.shape[1],1))

# In[16]:


callback = tf.keras.callbacks.EarlyStopping(monitor='loss',

                                 patience=200,
                                 min_delta=0.001, 
                                 mode='auto') 


# normalizer = tf.keras.layers.Normalization()
# normalizer.adapt(np.array(train_features))

# train_features

# first = np.array(train_features)
# 
# with np.printoptions(precision=2, suppress=True):
#   print('First example:', first)
#   print()
#   print('Normalized:', normalizer(first).numpy())

# In[ ]:


model = Sequential([
    layers.Input(shape= (4,1)),
    layers.BatchNormalization(),
    layers.Conv1D(filters = 16, kernel_size = 3, activation = 'relu'), 
    #layers.Dropout(0.5),
    #layers.LayerNormalization(),
    layers.GlobalAveragePooling1D(),
    layers.Dense(32, activation = 'relu', kernel_regularizer=regularizers.l2(0.001)),
    layers.Flatten(),
    layers.Dense(1)
])

model.compile(optimizer = keras.optimizers.Adam(learning_rate=0.003,  clipnorm = 1),#, amsgrad = True),
              loss = 'cosine_similarity', metrics =['mae', tf.keras.metrics.RootMeanSquaredError(
    name='root_mean_squared_error', dtype=None
)])

history = model.fit(train_features, train_label, epochs = 500, verbose =1, validation_split = .2, batch_size =16, callbacks = [callback])


# model = Sequential()
# model.add(normalizer())
# model.add(Conv1D(filters = 16, kernel_size = 3, activation = 'relu', input_shape = (4,1)))
# model.add(LayerNormalization())
# model.add(GlobalMaxPooling1D())
# #model.add(Flatten())
# model.add(Dense(128, activation = 'relu'))#, kernel_regularizer = keras.regularizers.L2(0.0001)))
# model.add(Dense(1))
# model.compile(optimizer = keras.optimizers.Adam(learning_rate = 0.0015, clipnorm =1), #, use_ema =True, amsgrad = True),
#               loss = 'cosine_similarity', metrics =['mae'])
# 
# history = model.fit(train_features, train_label, epochs = 400, verbose =1)#, callbacks = callback)

# In[ ]:


test_predictions = model.predict(train_features)


# In[ ]:


print(test_predictions)


# In[ ]:


lab_max = normeddata['gamma0'].max()
lab_min = normeddata['gamma0'].min()
lab_range = lab_max - lab_min
lab_mean = normeddata['gamma0'].mean()
print(lab_max, lab_min, lab_range, lab_mean)


# In[ ]:


pred_max = test_predictions[:, [0]].max()
pred_min = test_predictions[:, [0]].min()
pred_range = pred_max - pred_min
pred_mean = test_predictions[:, [0]].mean()
print(pred_max, pred_min, pred_range, pred_mean)


# In[ ]:


predictions_array_length = 40000
numcols = 10
array_sizes =  np.array([[0] * numcols ] * predictions_array_length,dtype='float64')
predictions = pd.DataFrame(array_sizes)
predictions.columns = [ 'gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted', 'gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted','logp2_cgs_predicted', 'gamma2_predicted']


# In[ ]:


predictions['gamma0_norm_predicted'] = test_predictions[:, [0]]
#predictions['logp1_cgs_norm_predicted'] = test_predictions[:, [1]]
#predictions['gamma1_norm_predicted'] = test_predictions[:, [2]]
#predictions['logp2_cgs_norm_predicted'] = test_predictions[:, [3]]
#predictions['gamma2_norm_predicted'] = test_predictions[:, [4]]
predictions


# In[ ]:


predictions['gamma0_predicted'] = test_predictions[:, [0]] * (5 - 1) + 1
#predictions['logp1_cgs_predicted'] = test_predictions[:, [1]] *  (37 - 33) + 33
#predictions['gamma1_predicted'] = test_predictions[:, [2]] * (5 - 1) + 1
#predictions['logp2_cgs_predicted'] = test_predictions[:, [3]] *  (37 - 33) + 33
#predictions['gamma2_predicted'] = test_predictions[:, [4]] * (5 - 1) + 1
predictions


# In[ ]:


error = test_predictions - train_label
plt.hist(error, bins=25)
plt.xlabel('Prediction Error')
_ = plt.ylabel('Count')


# In[ ]:


import numpy as np
import matplotlib.pyplot as plt

labels_cols = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']
norm_labels = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']

norm_predictions = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted']
predictions_cols = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']


labels = [df[col].to_numpy() for col in labels_cols]
normlabel = [normeddata[col].to_numpy() for col in norm_labels]

normpredictions = [predictions[col].to_numpy() for col in norm_predictions]
prediction = [predictions[col].to_numpy() for col in predictions_cols]


fig, axs = plt.subplots(5, 4, figsize=(15, 15))

for i in range(5):
    for j in range(4):
        if j == 0:  #first column i want to be gamma0_labels
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(labels[i], color=color, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Labels Unnormalized ({labels_cols[i]}) ")
            
        elif j == 1:  #second column should be the gamm0 predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normlabel[i], color=color,range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Labels ({norm_labels[i]}) ")
    
          
        elif j == 2:  #3rd column should be the norm labels 
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normpredictions[i], color=color,range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Pred. ({norm_predictions[i]}) ")
            
        
        elif j == 3: # 4th column should be the norm predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(prediction[i], color=color,range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Unnormalized Pred. ({predictions_cols[i]}) ")

plt.tight_layout()
plt.subplots_adjust(top=.94) 
fig.suptitle('predicting one output, cosine sim', fontsize=20)
plt.show()
plt.savefig('me.png')

# In[ ]:


import numpy as np
import matplotlib.pyplot as plt

labels_cols = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']
norm_labels = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']

norm_predictions = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted']
predictions_cols = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']


labels = [df[col].to_numpy() for col in labels_cols]
normlabel = [normeddata[col].to_numpy() for col in norm_labels]

normpredictions = [predictions[col].to_numpy() for col in norm_predictions]
prediction = [predictions[col].to_numpy() for col in predictions_cols]


fig, axs = plt.subplots(5, 4, figsize=(15, 15))

for i in range(5):
    for j in range(4):
        if j == 0:  #first column i want to be gamma0_labels
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(labels[i], color=color)#, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Labels Unnormalized ({labels_cols[i]}) ")
            
        elif j == 1:  #second column should be the gamm0 predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normlabel[i], color=color) #range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Labels ({norm_labels[i]}) ")
    
          
        elif j == 2:  #3rd column should be the norm labels 
            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(normpredictions[i], color=color)#, range=(np.min(normlabel[i]), np.max(normlabel[i])))
            axs[i, j].set_title(f"Normalized Pred. ({norm_predictions[i]}) ")
            
        
        elif j == 3: # 4th column should be the norm predictions

            color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
            axs[i, j].hist(prediction[i], color=color)#, range=(np.min(labels[i]), np.max(labels[i])))
            axs[i, j].set_title(f"Unnormalized Pred. ({predictions_cols[i]}) ")

plt.tight_layout()
plt.subplots_adjust(top=.94) 
fig.suptitle('predicting one output, cosine sim', fontsize=20)
plt.show()
plt.savefig('more.png')

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




