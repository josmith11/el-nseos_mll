#!/usr/bin/env python
# coding: utf-8

# In[139]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)


import scipy.interpolate as interp
#import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
logdir = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
shutil.rmtree(logdir, ignore_errors=True)

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#gpu configuration:

gpu_num = 1

gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
#import lal


# In[140]:


df = pd.read_csv('preprocessing-Copy1.csv', index_col = 0)
normeddata = df.copy(deep = True )


# In[141]:


class Normalizer:
    def __init__(self, df):
        self.df = df

    def normalize_columns(self, columns, min_val, max_val):
        for col in columns:
            self.df[col + '_norm'] = (self.df[col] - min_val) / (max_val - min_val)

    def normalize_by_min_max(self, columns):
        for col in columns:
            self.df[col + '_norm'] = (self.df[col] - self.df[col].min()) / (self.df[col].max() - self.df[col].min())

    def normalize_all(self):
        self.normalize_columns(['gamma0', 'gamma1', 'gamma2'], 1, 5)
        self.normalize_columns(['logp1_cgs', 'logp2_cgs'], 33, 37)
        self.normalize_by_min_max(['m1', 'm2', 'l1', 'l2'])
        return self.df
        
    def denormalize_columns(self, predictions, columns):
        denormalized_data = {}
        for i, col in enumerate(columns):
            if col in ['gamma0_predicted', 'gamma1_predicted', 'gamma2_predicted']:
                denormalized_data[col] = predictions[:, i] * (5 - 1) + 1  # Scale back to original range
            elif col in ['logp1_cgs_predicted', 'logp2_cgs_predicted']:
                denormalized_data[col] = predictions[:, i] * (37 - 33) + 33  # Scale back to original range
            else:
                raise ValueError(f"Unknown column for denormalization: {col}")
        return denormalized_data



normalizer = Normalizer(normeddata)
normeddata = normalizer.normalize_all()
normeddata = normeddata.drop(columns = ['m1', 'm2', 'l1', 'l2','gamma0', 'gamma1', 'gamma2', 'logp1_cgs', 'logp2_cgs'])


# In[142]:


smallnormed = normeddata.iloc[:1000]


# In[143]:


train_dataset = smallnormed.sample(frac=0.8, random_state=1)
test_dataset = smallnormed.drop(train_dataset.index)


# In[144]:


train_copy = train_dataset.copy()
test_copy = test_dataset.copy()


# In[145]:


train_label = train_copy.drop(columns = ['m1_norm', 'm2_norm', 'l1_norm', 'l2_norm']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 large training
#X train
test_label = test_copy.drop(columns = ['m1_norm', 'm2_norm', 'l1_norm', 'l2_norm']) #pandas with gamma0, gamma1, logp1, logp2, gamma2 small testing
#x test
train_features = train_copy.drop(columns = ['gamma0_norm', 'gamma1_norm', 'gamma2_norm', 'logp1_cgs_norm', 'logp2_cgs_norm']) #m1, m2, l1,l2 large
#Y_train
test_features = test_copy.drop(columns = ['gamma0_norm', 'gamma1_norm', 'gamma2_norm', 'logp1_cgs_norm', 'logp2_cgs_norm']) #m1,m2,l1,l2 small
#y_test


# In[151]:


import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from sklearn import datasets, ensemble
from sklearn.inspection import permutation_importance
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.utils.fixes import parse_version
from sklearn.multioutput import MultiOutputRegressor
from sklearn.datasets import make_regression
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import KFold, cross_val_score
from sklearn.metrics import make_scorer
from sklearn.metrics.pairwise import cosine_similarity


# params = {
#     "n_estimators": 1000,
#     "max_depth": 8,
#     "min_samples_split": 5,
#     "learning_rate": 0.01,
#     "loss": "huber",
#     "tol":1e-4,
#     "verbose": 1,
# } 

# params = {
#     "kernel": 'rbf', 
#     "tol": 1e-3,
#     "epsilon": .001,
#     'C': .5,
#     "verbose": 1,
#     "shrinking": True,
# }

# reg = MultiOutputRegressor(ensemble.GradientBoostingRegressor(**params))
# reg.fit(train_features, train_label)
# 
# #mse = mean_squared_error(train_label, reg.predict(train_features))
# #print("The mean squared error (MSE) on test set: {:.4f}".format(mse))

# In[152]:


svr = SVR()
mltout = MultiOutputRegressor(svr)
#mltouts = mltout.fit(train_features, train_label)
#test_predictions = mltouts.predict(train_features)


# In[170]:


param_grid = {
    'estimator__kernel': ('linear', 'poly', 'rbf', 'sigmoid'),
    'estimator__C': np.linspace(0.1, 10, 50),
    'estimator__degree': [2, 3, 4, 5],
    'estimator__coef0': [0.0, 0.1, 0.5],  
    'estimator__gamma': ('auto', 'scale'),
    'estimator__tol': [1e-3, 1e-4, 1e-5, 1e-6]
}


# In[171]:


def cosine_similarity_score(y_true, y_pred):
    # Calculate cosine similarity
    y_true_norm = np.linalg.norm(y_true, axis=1)[:, np.newaxis]
    y_pred_norm = np.linalg.norm(y_pred, axis=1)[:, np.newaxis]
    
    # Compute cosine similarity
    similarity = np.dot(y_true, y_pred.T) / (y_true_norm * y_pred_norm.T)
    return np.mean(similarity)


# In[173]:


from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV, KFold

cosine_scorer = make_scorer(cosine_similarity_score)
kfold_splitter = KFold(n_splits=5, random_state = 0,shuffle=True)

svr_random = GridSearchCV(mltout,
                  param_grid=param_grid,
                  #n_iter=20,
                  cv=kfold_splitter,
                  n_jobs=-1,
                  scoring=cosine_scorer, 
                  verbose = 1,) 
svr_random.fit(train_features, train_label)


print(svr_random.best_params_)


# predictions_array_length = 8000
# numcols = 10
# test_predictions = np.random.rand(predictions_array_length, 5) 
# 
# predictions_columns = [
#     'gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 
#     'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 
#     'gamma2_norm_predicted', 'gamma0_predicted', 
#     'logp1_cgs_predicted', 'gamma1_predicted',
#     'logp2_cgs_predicted', 'gamma2_predicted'
# ]
# 
# predictions = pd.DataFrame(np.zeros((predictions_array_length, numcols)), columns=predictions_columns)
# 
# normalized_cols = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 
#                    'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 
#                    'gamma2_norm_predicted']
# 
# for i, col in enumerate(normalized_cols):
#     predictions[col] = test_predictions[:, i]
# 
# #predictions[['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']] = test_predictions[:, :5]

# columns_to_denormalize = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']
# denormalized_predictions = normalizer.denormalize_columns(test_predictions, columns_to_denormalize)
# for col in columns_to_denormalize:
#     predictions[col] = denormalized_predictions[col]

# import numpy as np
# import matplotlib.pyplot as plt
# 
# labels_cols = ['gamma0', 'logp1_cgs', 'gamma1', 'logp2_cgs', 'gamma2']
# norm_labels = ['gamma0_norm', 'logp1_cgs_norm', 'gamma1_norm', 'logp2_cgs_norm', 'gamma2_norm']
# 
# norm_predictions = ['gamma0_norm_predicted', 'logp1_cgs_norm_predicted', 'gamma1_norm_predicted', 'logp2_cgs_norm_predicted', 'gamma2_norm_predicted']
# predictions_cols = ['gamma0_predicted', 'logp1_cgs_predicted', 'gamma1_predicted', 'logp2_cgs_predicted', 'gamma2_predicted']
# 
# 
# labels = [df[col].to_numpy() for col in labels_cols]
# normlabel = [normeddata[col].to_numpy() for col in norm_labels]
# 
# normpredictions = [predictions[col].to_numpy() for col in norm_predictions]
# prediction = [predictions[col].to_numpy() for col in predictions_cols]
# 
# 
# fig, axs = plt.subplots(5, 4, figsize=(15, 15))
# 
# for i in range(5):
#     for j in range(4):
#         if j == 0:  #first column i want to be gamma0_labels
#             color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
#             axs[i, j].hist(labels[i], color=color, range=(np.min(labels[i]), np.max(labels[i])))
#             axs[i, j].set_title(f"Labels Unnormalized ({labels_cols[i]}) ")
#             
#         elif j == 1:  #second column should be the gamm0 predictions
# 
#             color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
#             axs[i, j].hist(normlabel[i], color=color, range=(np.min(normlabel[i]), np.max(normlabel[i])))
#             axs[i, j].set_title(f"Normalized Labels ({norm_labels[i]}) ")
#     
#           
#         elif j == 2:  #3rd column should be the norm labels 
#             color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
#             axs[i, j].hist(normpredictions[i], color=color, range=(np.min(normlabel[i]), np.max(normlabel[i])))
#             axs[i, j].set_title(f"Normalized Pred. ({norm_predictions[i]}) ")
#             
#         
#         elif j == 3: # 4th column should be the norm predictions
# 
#             color = 'red' if i == 0 else 'darkorange' if i == 1 else 'gold' if i == 2 else 'green' if i == 3 else 'mediumblue'
#             axs[i, j].hist(prediction[i], color=color, range=(np.min(labels[i]), np.max(labels[i])))
#             axs[i, j].set_title(f"Unnormalized Pred. ({predictions_cols[i]}) ")
# 
# plt.tight_layout()
# plt.subplots_adjust(top=.94) 
# fig.suptitle('predicting one output, cosine sim', fontsize=20)
# plt.show()

# In[ ]:





# In[ ]:





# In[ ]:




