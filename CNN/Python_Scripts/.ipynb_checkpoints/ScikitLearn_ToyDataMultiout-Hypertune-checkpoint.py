#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

pd.set_option('display.max_columns', None)
#pd.set_option('display.max_rows', None)


import scipy.interpolate as interp
#import lalsimulation as lal
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import plot_model
import keras
from keras import metrics
import keras_metrics as km
from tensorflow.keras import layers

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers 
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
from  IPython import display
from matplotlib import pyplot as plt
import pathlib 
import shutil
import tempfile
logdir = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
shutil.rmtree(logdir, ignore_errors=True)

# Make NumPy printouts easier to read.
np.set_printoptions(precision=3, suppress=True)
#gpu configuration:

gpu_num = 1

gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        tf.config.experimental.set_visible_devices(gpus[gpu_num], 'GPU')
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")


# In[33]:


df = pd.read_csv('polynomial3.csv')


# In[34]:


df = df.iloc[:8000]
train_dataset = df.sample(frac=0.8, random_state=1)
test_dataset = df.drop(train_dataset.index)


# In[35]:


train_features = train_dataset.copy()
test_features = test_dataset.copy()


# In[36]:


train_featuress = train_features.drop(columns = ['a', 'b', 'c', 'd'])
test_featuress = test_features.drop(columns = ['a', 'b', 'c', 'd'])


# In[37]:


train_labels = train_features.drop(columns = ['x1', 'y1', 'x2', 'y2'])
test_labels = test_features.drop(columns = ['x1', 'y1', 'x2', 'y2'])


# params ={ 
#     'kernel': 'poly',
#     'tol': 1e-06,
#     'gamma': 'auto',
#     'epsilon':0.01,
#     'C': 1, 
#     'verbose': 0,
#     'coef0': 2, 
#     'degree':4,
# }

# In[50]:


from numpy import mean
from numpy import std
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedKFold
from sklearn.multioutput import MultiOutputRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import ExtraTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.svm import SVR
from sklearn.multioutput import RegressorChain
from sklearn.svm import LinearSVR
from numpy import absolute


# In[51]:


# only define either min_samples_split or min_samples_leaf
#max depth should be controled for overfitting
#with max_leaf_nodes then max_depth is ignored
#max_features, normally square root of total numnber of features works, higher values gives rise to overfitting 
#the lower the learning_rate the better
#n estimators can overfit the higher 
#subsample when slightly less than 1 makes the model robust by reducing the variance, values around 0.8 work well
#loss function to be minimized, look at regressor case
#init This can be used if we have made another model whose outcome is to be used as the initial estimates for GBM
#look into warm start and presort and also random state


# In[57]:


from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
ntrain_featuress_scaled = scaler.fit_transform(train_featuress)
train_featuressnorm = pd.DataFrame(ntrain_featuress_scaled, columns=train_featuress.columns)


ntest_featuress_scaled = scaler.fit_transform(test_featuress)
test_featuresssnorm = pd.DataFrame(ntest_featuress_scaled, columns=test_featuress.columns)
#print(ntrain_featuress_scaled.describe())


# In[32]:


import optuna
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split, cross_val_score, RepeatedKFold
from sklearn.metrics import mean_absolute_error
import numpy as np
import joblib  

def objective(trial):
#hyperparams
    param_space = {
        'n_estimators': trial.suggest_int('n_estimators', 200, 1000),  # number of boosting stages
        'learning_rate': trial.suggest_categorical('learning_rate', [0.008,0.01,0.012,0.014,0.016,0.018, 0.02]), 
        'max_depth': trial.suggest_int('max_depth', 3, 7),  # mximum depth of individual trees
        'subsample': trial.suggest_categorical('subsample', [0.5,0.6,0.7,0.8,1.0]),       
        'min_samples_split': trial.suggest_int('min_samples_split', 2, 20),  # min samples required to split a node
        'min_samples_leaf': trial.suggest_int('min_samples_leaf', 1, 10),  # min samples required at a leaf node
        'max_features': trial.suggest_categorical('max_features', ['sqrt', 'log2', None]),  # num of features to consider at each split
        'warm_start': trial.suggest_categorical('warm_start', [True, False]),  # reuse previous model's results?
        'tol': trial.suggest_float('tol', 1e-4, 1e-2, log=True),  # tol for tree growing
        'criterion': trial.suggest_categorical('criterion', ['friedman_mse', 'squared_error']),  # split quality criterion
        'loss': trial.suggest_categorical('loss', ['squared_error', 'absolute_error', 'huber', 'quantile']),  # loss function for optimization
      #  'alpha': trial.suggest_float('alpha', 0.9, 1.0)  # Quantile for Huber loss or other loss functions (0.9 for robust optimization)
    }


    gb_model = GradientBoostingRegressor(**param_space, random_state=42, validation_fraction = 0.1)

    regr = MultiOutputRegressor(gb_model)

    cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)
    n_scores = cross_val_score(regr, train_featuress, train_labels, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
    mae = np.mean(np.abs(n_scores))  
    return mae  

study = optuna.create_study( direction='minimize',storage="sqlite:///db.sqlite3", study_name = 'multioutregrhyp')
study.optimize(objective, n_trials=50)


best_params_optuna = study.best_params
print("Best Hyperparameters:", best_params_optuna)

best_model_optuna = GradientBoostingRegressor(**best_params_optuna, random_state=42)


best_regr_optuna = MultiOutputRegressor(best_model_optuna)

best_regr_optuna.fit(train_featuress, train_labels)

y_pred_best_optuna = best_regr_optuna.predict(test_featuress)

mae_best_optuna = mean_absolute_error(y_test, y_pred_best_optuna)
#mae for each output
#mae_per_output = mean_absolute_error(y_test, y_pred_best_optuna, multioutput='raw_values')
#print("MAE per output:", mae_per_output)

print(f"Best Model MAE (Optuna): {mae_best_optuna:.4f}")
joblib.dump(best_regr_optuna, 'best_gbr_model_optuna.joblib')
print("Model saved as 'best_gbr_model_optuna.joblib'")


# from sklearn.multioutput import MultiOutputRegressor
# from sklearn.svm import SVR
# from sklearn.metrics import r2_score
# import pandas as pd
# 
# df_models = pd.DataFrame(data=None, columns=['Algorithm', 'r2_train', 'r2_test'])
# 
# def make_model(train_featuressnorm, test_featuress, train_labels, test_labels, model, model_name: str): 
#     model.fit(train_featuressnorm, train_labels)
#     y_pred_train = model.predict(train_featuressnorm)
#     y_pred_test = model.predict(test_featuress)
#     
#     r2_train = r2_score(train_labels, y_pred_train)
#     r2_test = r2_score(test_labels, y_pred_test)
#     
#     df_models.loc[len(df_models.index)] = [model_name, r2_train, r2_test]
# 
# multi_output_svr = MultiOutputRegressor(SVR())
# 
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, multi_output_svr, 'MultiOutput_SVR')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, LinearRegression(), 'LinearRegression')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, Ridge(), 'Ridge')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, Lasso(), 'Lasso')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, ElasticNet(), 'ElasticNet')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, GradientBoostingRegressor(), 'GradientBoosting')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, RandomForestRegressor(), 'RandomForest')
# make_model(train_featuressnorm, test_featuress, train_labels, test_labels, XGBRegressor(), 'XGBoost')
# print(df_models)
# 

# from sklearn.multioutput import MultiOutputRegressor
# from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
# from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
# #from xgboost import XGBRegressor
# from sklearn.metrics import r2_score
# import pandas as pd
# 
# # Initialize the dataframe to store the results
# df_models = pd.DataFrame(data=None, columns=['Algorithm', 'r2_train', 'r2_test'])
# 
# # Define the function to fit models and store R2 results
# def make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, model, model_name: str): 
#     model.fit(train_featuressnorm, train_labels)
#     y_pred_train = model.predict(train_featuressnorm)
#     y_pred_test = model.predict(test_featuresssnorm)
#     
#     r2_train = r2_score(train_labels, y_pred_train)
#     r2_test = r2_score(test_labels, y_pred_test)
#     
#     df_models.loc[len(df_models.index)] = [model_name, r2_train, r2_test]
# 
# # Wrap each model in MultiOutputRegressor and evaluate
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(LinearRegression()), 'LinearRegression')
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(Ridge()), 'Ridge')
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(Lasso()), 'Lasso')
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(ElasticNet()), 'ElasticNet')
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(GradientBoostingRegressor()), 'GradientBoosting')
# make_model(train_featuressnorm, test_featuresssnorm, train_labels, test_labels, MultiOutputRegressor(RandomForestRegressor()), 'RandomForest')
# #make_model(train_featuress, test_featuress, train_labels, test_labels, MultiOutputRegressor(XGBRegressor()), 'XGBoost')
# 
# # Print the results
# print(df_models)
# 

# test_labels = test_labels[['b', 'a', 'c', 'd']]

# predictions_array_length = len(test_predictions[:,[0]])
# numcols = 4
# predictions_columns = [
#     'a', 'b', 
#     'c', 'd', 
# ]
# predictions = pd.DataFrame(np.zeros((predictions_array_length, numcols)), columns=predictions_columns)
# for i, col in enumerate(predictions_columns):
#     predictions[col] = test_predictions[:, i]

# import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
# 
# def plot_distributions_and_scatter(labels_df, predictions, labels_columns, colors=None, scatter_xyrange=None,dist_xrange=None):
#     num_outputs = len(labels_columns)
#     fig, axs = plt.subplots(2, num_outputs, figsize=(16, 8))  
# 
#     if colors is None:
#         colors = ['tomato', 'darkorange', 'pink', 'skyblue', 'darkblue', 'green', 'purple', 'brown']
#         
#     for i, label in enumerate(labels_columns):
#         color = colors[i % len(colors)]
#         
#         axs[0, i].hist(labels_df[label], color=color, alpha=0.6, label="Labels", range=(np.min(labels_df[label]), np.max(labels_df[label])))
#         axs[0, i].hist(predictions[label], edgecolor=color, fill=False, linewidth=1.5, label="Predictions", range=(np.min(labels_df[label]), np.max(labels_df[label])))
#         axs[0, i].set_title(f"Distribution of {label}")
#         axs[0, i].set_xlabel(f"Value of {label}")
#         axs[0, i].set_ylabel("Frequency")
#         axs[0, i].legend()
#         
#         axs[1, i].scatter(labels_df[label], predictions[label], marker='.', color=color, alpha=0.6)
#         slope, intercept = np.polyfit(labels_df[label], predictions[label], 1)
#         axs[1, i].plot(labels_df[label], slope * labels_df[label] + intercept, color='red', label='Best fit')
#         
#         if scatter_xyrange:
#             axs[1, i].set_xlim(scatter_xyrange)
#             axs[1, i].set_ylim(scatter_xyrange)
# 
#         min_val = min(np.min(labels_df[label]), np.min(predictions[label]))
#         max_val = max(np.max(labels_df[label]), np.max(predictions[label]))
#         axs[1, i].plot([min_val, max_val], [min_val, max_val], color='purple', linestyle='--', label='y = x')
# 
#         std_dev = np.std(predictions[label])
#         axs[1, i].set_title(f"Scatter Plot: True vs Predicted {label}")
#         axs[1, i].set_xlabel(f"True Values of {label}")  
#         axs[1, i].set_ylabel(f"Predicted Values of {label}")
#         axs[1, i].legend(title=f'Std Dev: {std_dev:.2f}')
#     
#     plt.tight_layout()
#     plt.subplots_adjust(top=0.9)
#     fig.suptitle('Label vs Prediction Distributions and Scatter Plots', fontsize=16)
#     plt.show()
# 
# labels_columns = ['a', 'b', 'c', 'd'] 
# labels_df = test_labels  
# plot_distributions_and_scatter(labels_df, predictions, labels_columns,  scatter_xyrange=(0, 1))
# 

# In[ ]:




