import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def toyproblemcurves(labels, predictions):

     """
    Plots polynomial curves derived from labels and predictions.

    Parameters:
    - labels (pd.DataFrame): pandas dataframe containing the true values of the coefficents (columns: 'a', 'b', 'c', 'd').
    - predictions (pd.DataFrame): dataframe containing the predicted values of the coefficents (columns: 'a', 'b', 'c', 'd').
    """


    x = np.linspace(0, 1, 100)
    
    max_curves = min(1600, len(predictions), len(labels_df)) 
    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
    
    for i in range(max_curves):
        row = labels_df.iloc[i]

        a, b, c, d = row['a'], row['b'], row['c'], row['d']
        y = a + b * x + c * x**2 + d * x**3
        ax1.plot(x, y, linestyle='-', alpha = 0.1, color = 'blue')  
    
    ax1.set_title('poly curves from labels')
    ax1.set_xlim(0,1)
    ax1.set_ylim(0,4)
    
    ax1.set_xlabel('x')
    ax1.set_ylabel('y(x)')
    ax1.grid()
    
    for i in range(max_curves):
        row = predictions.iloc[i]
        a, b, c, d = row['a'], row['b'], row['c'], row['d']
        y = a + b * x + c * x**2 + d * x**3
        ax2.plot(x, y, linestyle='-', alpha = 0.1, color = 'purple') 
    
    ax2.set_title('poly curves from predictions')
    ax2.set_xlabel('x')
    ax2.set_ylabel('y(x)')
    ax2.set_xlim(0,1)
    ax2.set_ylim(0,4)
    
    ax2.grid()
    plt.tight_layout()
    plt.show()
    
