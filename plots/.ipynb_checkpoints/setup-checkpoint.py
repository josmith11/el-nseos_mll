# setup.py

from setuptools import setup, find_packages

setup(
    name='plots',  #unbelievably unneccesary but isnt this fun
    version='0.1',
    packages=find_packages(),
    install_requires=[  
        'numpy',
        'pandas',
        'matplotlib',
    ],
    description="Function for making histograms and scatterplots, callable anywhere should hopefully work with any number of variables to be used for toy problem and beyond.",
    long_description="This package contains a utility function for creating beautiful scatter plots and histograms.",
    author="Josephine Smith",
    author_email="smith26@kenyon.edu",
    url="https://gitlab.com/josmith11/plots",
    classifiers=[ #got this from the internet so we will see
      "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)

