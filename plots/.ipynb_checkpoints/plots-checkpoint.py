import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def my_beautiful_scatter_plots(labels, predictions, column_names, colors=None, scatter_xyrange=None, dist_xrange=None, title="Label vs Prediction Distributions and Scatter Plots"):
    """
    Plots distributions and scatter plots for given labels and predictions, without using GridSpec, using subplots for simplicity.

    Parameters:
    - labels (pd.DataFrame): DataFrame containing true label values.
    - predictions (pd.DataFrame): DataFrame containing predicted values.
    - column_names (list): List of column names in `labels` and `predictions` to plot.
    - colors (list, optional): List of colors for plotting, default is nice pastel color scheme.
    - scat=ter_xyrange (tuple, optional): Range for x and y axis of scatter plots. Default is 'None' goes to the min and max of labels/predictions.
    - dist_xrange (tuple, optional): Range for x axis of histograms. Default is None.
    - title (str, optional): Title for the entire plot. Default is 'Label vs Prediction Distributions and Scatter Plots'.
    """
    
    # Number of plots
    num_outputs = len(column_names)
    
    # Create figure and axes for subplots
    fig, axes = plt.subplots(nrows=num_outputs, ncols=2, figsize=(12, num_outputs * 6), constrained_layout=True)
    
    # Set default colors if not provided
    if colors is None:
        colors = ['tomato', 'darkorange', 'pink', 'skyblue', 'darkblue', 'green', 'purple', 'brown']
    
    reference_line_color = 'purple'
    
    for i, label in enumerate(column_names):
        # Assign axes for histogram and scatter plot
        ax_hist = axes[i, 0]  # First column for histograms
        ax_scatter = axes[i, 1]  # Second column for scatter plots
        
        # Histogram plot for label vs prediction distribution
        ax_hist.hist(labels[label], color=colors[i % len(colors)], alpha=0.6, label="Labels", range=dist_xrange if dist_xrange else (np.min(labels[label]), np.max(labels[label])))
        ax_hist.hist(predictions[label], edgecolor=colors[i % len(colors)], fill=False, linewidth=1.5, label="Predictions", range=dist_xrange if dist_xrange else (np.min(labels[label]), np.max(labels[label])))
        ax_hist.set_title(f"Distribution of {label}")
        ax_hist.set_xlabel(f"Value of {label}")
        ax_hist.set_ylabel("Frequency")
        ax_hist.legend()
        
        ax_hist.set_aspect('auto')  
        # Scatter plot for true vs predicted values
        ax_scatter.scatter(labels[label], predictions[label], marker='.', color=colors[i % len(colors)], alpha=0.6)
        slope, intercept = np.polyfit(labels[label], predictions[label], 1)
        ax_scatter.plot(labels[label], slope * labels[label] + intercept, color='red', label='Best fit')
        
        if scatter_xyrange:
            ax_scatter.set_xlim(scatter_xyrange)
            ax_scatter.set_ylim(scatter_xyrange)

        # Plot a reference line y=x
        min_val = min(np.min(labels[label]), np.min(predictions[label]))
        max_val = max(np.max(labels[label]), np.max(predictions[label]))
        ax_scatter.plot([min_val, max_val], [min_val, max_val], color=reference_line_color, linestyle='--', label='y = x')

        # Calculate and display standard deviation in the legend
        std_dev = np.std(predictions[label])
        ax_scatter.set_title(f"Scatter Plot: True vs Predicted {label}")
        ax_scatter.set_xlabel(f"True Values of {label}")  
        ax_scatter.set_ylabel(f"Predicted Values of {label}")
        ax_scatter.legend(title=f'Std Dev: {std_dev:.2f}')
        
        # Make scatter plot square by setting aspect ratio
        ax_scatter.set_aspect('equal')  # Ensure the axes are equal to make it square
    
    # Add a super title for the whole figure
    fig.suptitle(title, fontsize=16)
    
    # Show the plot
    plt.show()


